package com.example.activity_lifecycle

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.SeekBar

const val CURRENT_B = "current b"
class ActivityB : AppCompatActivity() {
    private lateinit var seekbar: SeekBar
    private lateinit var btn: Button
    private lateinit var mediaPlayer: MediaPlayer


    private lateinit var runnable: Runnable
    private var handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"Activity B onCreate")

        setContentView(R.layout.activity_b)

        btn = findViewById(R.id.button1)

        mediaPlayer = MediaPlayer.create(this,R.raw.b)


        btn.setOnClickListener{
            finish()
        }

        if(savedInstanceState != null){
            mediaPlayer.seekTo(savedInstanceState.getInt(CURRENT_B))
        }

        seekbar = findViewById(R.id.seekbar1)
        seekbar.progress = mediaPlayer.currentPosition
        seekbar.max = mediaPlayer.duration
        mediaPlayer.start()

        runnable = Runnable {
            seekbar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable,1000)
        }
        handler.postDelayed(runnable,1000)
        mediaPlayer.setOnCompletionListener {
            mediaPlayer.pause()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Activity B onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Activity B onResume")
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.pause()
        Log.d(TAG,"Activity B onPause ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Activity B onStop")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_B,mediaPlayer.currentPosition)
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
        Log.d(TAG,"Activity B onDestroy change to Activity A")
    }
}