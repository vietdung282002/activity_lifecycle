package com.example.activity_lifecycle

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.SeekBar

const val TAG = "Activity Lifecycle"
const val CURRENT_A = "current a"

class MainActivity : AppCompatActivity() {
    private lateinit var seekbar: SeekBar
    private lateinit var btn: Button
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var runnable: Runnable
    private var current_b = 0
    private var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"Activity A onCreate")
        setContentView(R.layout.activity_main)
        seekbar = findViewById(R.id.seekbar)
        btn = findViewById(R.id.button)
        mediaPlayer = MediaPlayer.create(this,R.raw.a)
        seekbar.max = mediaPlayer.duration

        btn.setOnClickListener{
            val intent = Intent(this,ActivityB::class.java)
            Log.d(TAG,"Activity A change to Activity B")
            startActivity(intent)
        }

        if(savedInstanceState != null){
            mediaPlayer.seekTo(savedInstanceState.getInt(CURRENT_A))
        }

        seekbar.progress = mediaPlayer.currentPosition

        mediaPlayer.start()

        runnable = Runnable {
            seekbar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable,1000)
        }
        handler.postDelayed(runnable,1000)
        mediaPlayer.setOnCompletionListener {
            mediaPlayer.pause()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Activity A onStart")
    }
    
    override fun onResume() {
        super.onResume()
        mediaPlayer.start()
        Log.d(TAG,"Activity A onResume")
    }
    override fun onRestart() {
        super.onRestart()
        Log.d(TAG,"Activity A Restart")
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.pause()
        Log.d(TAG,"Activity A onPause ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Activity A onStop")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_A,mediaPlayer.currentPosition)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"Activity A onDestroy")
        mediaPlayer.stop()
    }
}